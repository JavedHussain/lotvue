class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  def index
    @users = User.all
  end

  # GET /users/1
  def show
  end

  # GET /users/new
  def new
    @user = User.new
    @images = @user.images.build
    @user_meta_infos = @user.user_meta_infos
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  def create
    @user = User.new(first_name: user_params[:first_name], last_name: user_params[:last_name],
      email: user_params[:email], role:Role.find(user_params[:role]))

    if @user.save
      redirect_to @user, notice: 'User was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      redirect_to @user, notice: 'User was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
    redirect_to users_url, notice: 'User was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.where(id: params[:id]).first
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:first_name, :last_name, :email, :role, 
        images_attributes: [:id, :user_id, :avatar],
        user_meta_infos_attributes: [:id, :user_id, :meta_key, :meta_value]
        )
    end
end
