class User < ApplicationRecord
	belongs_to :role
	has_many :user_meta_infos, dependent: :destroy
	has_many :images, dependent: :destroy

	accepts_nested_attributes_for :images, :user_meta_infos

end
