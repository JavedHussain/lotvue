class CreateUserMetaInfos < ActiveRecord::Migration[6.0]
  def change
    create_table :user_meta_infos, id: :uuid do |t|
      t.references :user, null: false, type: :uuid, foreign_key: true
      t.string :meta_key
      t.string :meta_value

      t.timestamps
    end
  end
end
