class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users, id: :uuid do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.references :role, null: false, type: :uuid, foreign_key: true

      t.timestamps
    end
    add_index :users, :email
  end
end
