class CreateImages < ActiveRecord::Migration[6.0]
  def change
    create_table :images, id: :uuid do |t|
      t.references :user, null: false, type: :uuid, foreign_key: true
      t.string :avatar

      t.timestamps
    end
  end
end
