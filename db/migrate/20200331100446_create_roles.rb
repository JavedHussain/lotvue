class CreateRoles < ActiveRecord::Migration[6.0]
  def change
    create_table :roles, id: :uuid do |t|
      t.string :name
      t.text :description
      t.boolean :status

      t.timestamps
    end
    add_index :roles, :name
    add_index :roles, :status
  end
end
