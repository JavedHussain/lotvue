Rails.application.routes.draw do
  resources :users
  resources :roles do
  	collection do
  		patch 'update_multiple'
  	end
  end
  root to: 'home#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
